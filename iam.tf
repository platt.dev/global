locals {
  tf-k8s-roles = [
    "compute.viewer",
    "compute.securityAdmin",
    "container.clusterAdmin",
    "container.developer",
    "iam.serviceAccountAdmin",
    "iam.serviceAccountUser",
    "resourcemanager.projectIamAdmin"
  ]
}

resource "google_service_account" "tf-k8s" {
  account_id   = "tf-k8s"
  display_name = "tf-k8s"
  description  = "Account used to terraform kubernetes clusters"
}

resource "google_project_iam_binding" "tf-k8s" {
  for_each = toset(local.tf-k8s-roles)
  role     = "roles/${each.value}"

  members = [
    "serviceAccount:${google_service_account.tf-k8s.email}",
  ]
}

output "tf-k8s-key" {
  value = "To create a service account key run: gcloud iam service-accounts keys create tf-k8s.json --iam-account=${google_service_account.tf-k8s.email}"
}
