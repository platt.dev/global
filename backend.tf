terraform {
  required_version = "0.12.26"

  backend "gcs" {
    bucket  = "tf-state-platt-dev"
    prefix  = "iam"
  }
}
