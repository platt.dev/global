# k8s_cluster


Manage iam


# Bootsrap
* Create a google storage bucket for the teraform state file
```
export BUCKET="tf-state-platt-dev"
gsutil mb -b on -l europe-west2 -p platt-dev "gs://${BUCKET}/"
gsutil versioning set on "gs://${BUCKET}/"
```


# To use
```
terraform init
terraform apply
```
