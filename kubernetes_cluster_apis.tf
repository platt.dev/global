resource "google_project_service" "services" {
  for_each = toset(["compute", "cloudresourcemanager", "container"])

  service                    = "${each.value}.googleapis.com"
  disable_dependent_services = true
  disable_on_destroy         = true
}

resource "google_storage_bucket" "tf-state-platt-dev-k8s" {
  name          = "tf-state-platt-dev-k8s"
  location      = "europe-west2"
  force_destroy = true

  bucket_policy_only = true
  versioning {
    enabled = true
  }
}

resource "google_storage_bucket_iam_member" "tf-k8s" {
  bucket = google_storage_bucket.tf-state-platt-dev-k8s.name
  role   = "roles/storage.legacyBucketWriter"
  member = "serviceAccount:${google_service_account.tf-k8s.email}"
}
